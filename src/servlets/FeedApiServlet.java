package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import beans.Article;
import beans.Feed;
import handlers.FeedApiHandler;
import jdk.nashorn.internal.parser.JSONParser;

/**
 * Servlet implementation class FeedApiServlet
 */
@WebServlet("/FeedApiServlet")
public class FeedApiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public FeedApiServlet() {
    	super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject obj = new JSONObject();
		try{
			String cmd = request.getParameter("cmd");
			String userId = request.getParameter("userid");
			if(cmd.equals("getUserFeeds")){
				List<Feed> result = FeedApiHandler.getAllFeedsForSubscriber(userId);
				try {
					JSONArray feedsJson = new JSONArray();
					for(Feed f: result){
						JSONObject fjson = new JSONObject();
						fjson.put("name", f.getName());
						fjson.put("id", f.getId());
						feedsJson.put(fjson);
					}
					obj.put("feeds", feedsJson);
					obj.put("userID", userId);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}else if(cmd.equals("getUserArticles")){
				List<Article> result = FeedApiHandler.getAllArticlesForSubscriber(userId);
				try {
					JSONArray feedsJson = new JSONArray();
					for(Article a: result){
						JSONObject fjson = new JSONObject();
						fjson.put("title", a.getTitle());
						fjson.put("id", a.getId());
						fjson.put("url", a.getUrl());
						fjson.put("feedID", a.getFeedID());
						feedsJson.put(fjson);
					}
					obj.put("articles", feedsJson);
					obj.put("userID", userId);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			else {
				obj.put("error", "unknown_command");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
		}
		catch(Exception e) {
			try {
				obj.put("error", "server_error");
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(obj.toString());
		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject obj = new JSONObject();
		try{
			response.setContentType("application/json");
			  StringBuffer jb = new StringBuffer();
			  String line = null;
			  try {
			    BufferedReader reader = request.getReader();
			    while ((line = reader.readLine()) != null)
			      jb.append(line);
			  } catch (Exception e) { /*report an error*/ }

			JSONObject jObject  = new JSONObject(jb.toString());
			String cmd = jObject.getString("cmd");			
			String feedId = jObject.getString("feedid");
			if(cmd.equals("subscribeUserFeed")){
				String userId = jObject.getString("userid");
				try {
					boolean succeeded = FeedApiHandler.subscribeUserFeed(userId,feedId);				
					try {
						obj.put("subscribtion succeded", succeeded);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(cmd.equals("unsubscribeUserFeed")){
				String userId = jObject.getString("userid");
				try {
					boolean succeeded = FeedApiHandler.unsubscribeUserFeed(userId,feedId);				
					try {
						obj.put("unsubscribtion succeded", succeeded);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(cmd.equals("addArticleToFeed")){
				String articleTitle = jObject.getString("article_title");
				String url = jObject.getString("article_url");
				try {
					boolean succeeded = FeedApiHandler.addArticleToFeed(articleTitle,feedId,url);				
					try {
						obj.put("add article succeded", succeeded);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				obj.put("error", "unknown_command");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
		}
		catch(Exception e) {
			try {
				obj.put("error", "server_error");
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		PrintWriter out = response.getWriter();
		out.print(obj.toString());
		out.flush();
	}

}
