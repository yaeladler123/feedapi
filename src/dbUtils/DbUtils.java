package dbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Article;
import beans.Feed;

public class DbUtils {
	
	private final static String GET_FEEDS = "Select f.NAME, f.FEED_ID "
			+ "from FEED f "
		   	+ "join USER_FEED uf on uf.FEED_ID = f.FEED_ID "
		   	+ "where uf.USER_ID = ?";
	
	private final static String GET_ARTICLES = "Select a.ARTICLE_ID , a.TITLE, a.URL, a.FEED_ID "
	   		+ "from ARTICLE a "
	   		+ "join USER_FEED uf on uf.FEED_ID = a.FEED_ID "
	   		+ "where uf.USER_ID = ?";
	
	private final static String CHECK_USERID = "Select 1 from USERS where USER_ID = ?";
	
	private final static String CHECK_FEEDID = "Select 1 from FEED where FEED_ID = ?";
	
	private final static String INSERT_USER_FEED = "INSERT INTO USER_FEED "
			+ "(USER_ID,FEED_ID) VALUES (?,?)";
	
	private final static String DELETE_USER_FEED = "DELETE FROM USER_FEED "
			+ "WHERE USER_ID = ? AND FEED_ID = ?";
	
	private final static String INSERT_ARTICLE = "INSERT INTO ARTICLE "
			+ "(TITLE,FEED_ID,URL) VALUES (?,?,?)";
	
	public static List<Feed> getFeedsForSubscriber(String subscriberId,Connection conn) throws SQLException {
		   PreparedStatement sql = conn.prepareStatement(GET_FEEDS);
	       sql.setString(1, subscriberId);	 
	       ResultSet rs = sql.executeQuery();
	       List<Feed> list = new ArrayList<Feed>();
	       while (rs.next()) {
	           int feedID = rs.getInt("FEED_ID");
	           String name = rs.getString("NAME");
	           Feed feed = new Feed(feedID,name);
	           list.add(feed);
	       }
	       return list;
	   }
	
	public static List<Article> getArticlesForSubscriber(String subscriberId,Connection conn) throws SQLException {
		   PreparedStatement sql = conn.prepareStatement(GET_ARTICLES);
	       sql.setString(1, subscriberId);	 
	       ResultSet rs = sql.executeQuery();
	       List<Article> list = new ArrayList<Article>();
	       while (rs.next()) {
	    	   int articleID = rs.getInt("ARTICLE_ID");
	    	   String title = rs.getString("TITLE");
	    	   String url = rs.getString("URL");
	           int feedID = rs.getInt("FEED_ID");
	           Article article = new Article(articleID, title, url, feedID);
	           list.add(article);
	       }
	       return list;
	   }
	
	public static boolean subscribeUserFeed(String userId, String feedId, Connection conn) throws SQLException{
		PreparedStatement sql = conn.prepareStatement(CHECK_USERID);
	    sql.setString(1, userId);	 
	    ResultSet rs = sql.executeQuery();
	    PreparedStatement sql2 = conn.prepareStatement(CHECK_FEEDID);
	    sql2.setString(1, feedId);	 
	    ResultSet rs2 = sql2.executeQuery();
	    if(!rs.next() || !rs2.next()){
	    	return false;
	    }
	    PreparedStatement sqlInsert = conn.prepareStatement(INSERT_USER_FEED);
	    sqlInsert.setString(1, userId);
	    sqlInsert.setString(2, feedId);	 
	    sqlInsert.executeQuery();
	    return true;	    
	}
	
	public static boolean unsubscribeUserFeed(String userId, String feedId, Connection conn) throws SQLException{
		PreparedStatement sql = conn.prepareStatement(CHECK_USERID);
	    sql.setString(1, userId);	 
	    ResultSet rs = sql.executeQuery();
	    PreparedStatement sql2 = conn.prepareStatement(CHECK_FEEDID);
	    sql2.setString(1, feedId);	 
	    ResultSet rs2 = sql2.executeQuery();
	    if(!rs.next() || !rs2.next()){
	    	return false;
	    }
	    
	    PreparedStatement sqlDelete = conn.prepareStatement(DELETE_USER_FEED);
	    sqlDelete.setString(1, userId);
	    sqlDelete.setString(2, feedId);	 
	    int affectedRows = sqlDelete.executeUpdate();
	    return affectedRows > 0;    
	}
	
	public static boolean addArticleToFeed(String articleTitle, String feedId, String url, Connection conn) throws SQLException{
	    PreparedStatement sql = conn.prepareStatement(CHECK_FEEDID);
	    sql.setString(1, feedId);	 
	    ResultSet rs = sql.executeQuery();
	    if(!rs.next()){
	    	return false;
	    }
	    PreparedStatement sqlInsert = conn.prepareStatement(INSERT_ARTICLE);
	    sqlInsert.setString(1, articleTitle);
	    sqlInsert.setString(2, feedId);
	    sqlInsert.setString(3, url);
	    sqlInsert.executeQuery();
	    return true;	    
	}

}
