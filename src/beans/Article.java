package beans;

public class Article {
	
	private int id;
	private String title;
	private String url;
	private int feedID;
	
	public Article(){
		
	}
	
	public Article(int id, String title, String url, int feedID){
		this.id = id;
		this.title = title;
		this.url = url;
		this.feedID = feedID;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFeedID() {
		return feedID;
	}
	public void setFeedID(int feedID) {
		this.feedID = feedID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	

}
