package handlers;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import beans.Article;
import beans.Feed;
import dbConnection.ConnectionUtils;
import dbUtils.DbUtils;

public class FeedApiHandler {
	
	public static List<Feed> getAllFeedsForSubscriber(String subscriberId){
		Connection conn = null;
		List<Feed> list = null;
		try {
			conn = ConnectionUtils.getConnection();
			list = DbUtils.getFeedsForSubscriber(subscriberId,conn);
		} catch (Exception e1) {
			e1.printStackTrace();
			ConnectionUtils.rollbackQuietly(conn);			
		}
        return list;	
	}
	
	public static List<Article> getAllArticlesForSubscriber(String subscriberId){
		Connection conn = null;
		List<Article> list = null;
		try {
			conn = ConnectionUtils.getConnection();
			list = DbUtils.getArticlesForSubscriber(subscriberId,conn);
		} catch (Exception e1) {
			e1.printStackTrace();
			ConnectionUtils.rollbackQuietly(conn);			
		}
        return list;		
	}
	
	public static boolean subscribeUserFeed(String userID, String feedID) throws SQLException{
		Connection conn = null;
		try {
			conn = ConnectionUtils.getConnection();
			boolean succeeded = DbUtils.subscribeUserFeed(userID, feedID, conn);
			return succeeded;
		} catch (Exception e1) {
			e1.printStackTrace();
			ConnectionUtils.rollbackQuietly(conn);	
			return false;
		}		
	}
	
	public static boolean unsubscribeUserFeed(String userID, String feedID) throws SQLException{
		Connection conn = null;
		try {
			conn = ConnectionUtils.getConnection();
			boolean succeeded = DbUtils.unsubscribeUserFeed(userID, feedID, conn);
			return succeeded;
		} catch (Exception e1) {
			e1.printStackTrace();
			ConnectionUtils.rollbackQuietly(conn);	
			return false;
		}		
	}
	
	public static boolean addArticleToFeed(String articleTitle,String feedId,String url) throws SQLException{
		Connection conn = null;
		try {
			conn = ConnectionUtils.getConnection();
			boolean succeeded = DbUtils.addArticleToFeed(articleTitle, feedId, url, conn);
			return succeeded;
		} catch (Exception e1) {
			e1.printStackTrace();
			ConnectionUtils.rollbackQuietly(conn);	
			return false;
		}		
	}

}
