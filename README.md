# FeedAPI #

* The API is configured to run on Tomcat server.
* The database used is Oracle SQL. [Here](https://bitbucket.org/yaeladler123/feedapi/src/af20bcfe88bc5198a04177cc050885955e031164/feedapidb.sql?at=master&fileviewer=file-view-default) is the DB dump to create the schema and some seed data.
* It has one endpoint handling all the requests: http://localhost:8080/FeedApiProject/
* All responses returned have a JSON body.
* All POST requests expect a JSON body.

# Operations supported #

## Add	Articles to a Feed ##

Verb: POST

Request JSON:

```
#!javascript

{
  "cmd": "addArticleToFeed",
  "article_url": "http://www.example.com",
  "article_title": "example title",
  "feedid": "4"
}
```


Response JSON:

```
#!javascript

{
  "add article succeeded": true
}
```


## Subscribe/Unsubscribe a User	to a Feed ##

Verb: POST

Request JSON for subscribe:

```
#!javascript

{
  "cmd": "subscribeUserFeed",
  "userid": "3",
  "feedid": "1"
}

```


Request JSON for unsubscribe:

```
#!javascript

{
  "cmd": "unsubscribeUserFeed",
  "userid": "3",
  "feedid": "1"
}
```


Response JSON:

```
#!javascript

{
  "unsubscribtion succeded": true
}
```


## Get all Feeds a Subscriber is following ##

Verb: GET

Parameteres: ?cmd=getUserFeeds&userid=1

Response JSON:

```
#!javascript

{
  "feeds": [
    {
      "name": "TWITTER",
      "id": 2
    },
    {
      "name": "STACKOVERFLOW",
      "id": 3
    }
  ],
  "userID": "1"
}
```


## Get	Articles from the set of Feeds a Subscriber is following ##


Verb: GET

Parameteres: ?cmd=getUserArticles&userid=1

Response JSON:

```
#!javascript

{
  "articles": [
    {
      "feedID": 3,
      "id": 2,
      "title": "CLINTON",
      "url": "www.clinton.com"
    },
    {
      "feedID": 2,
      "id": 4,
      "title": "BARAK",
      "url": "wwww.barak.com"
    },
    {
      "feedID": 2,
      "id": 5,
      "title": "SHARON",
      "url": "www.sharon.com"
    },
    {
      "feedID": 2,
      "id": 6,
      "title": "OBAMA",
      "url": "www.obama.com"
    }
  ],
  "userID": "1"
}
```


## Errors ##

If an error occurs the API will return a JSON with the key "error" and it's value will try to explain the problem.
Example:

```
#!javascript

{
  "error": "unknown_command"
}
```